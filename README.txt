Source code for Cloud Discovery page demo.

# 
 # Instructions

1. This demo relies on a Neo4j database backend. Download and install neo4j community and transfer the ipdemo.db folder in this repo to /path/to/your/neo4j/installation/data/databases and rename it to graph.db.

You will have to change line 554 of ipdemo.js if your neo4j instance is not running on localhost.

Run neo4j with the command './bin/neo4j start'.

2. Start the demo with the command

 > python -m SimpleHTTPServer 8080

Then you can navigate to http://localhost:8080 in your web browser to see the demo running