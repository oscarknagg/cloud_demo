// onresize gets called a lot in some browsers, 
// so use this debounce function to rate limit the call to resize
var debounce = function (func) {
  var timeout;

  return function debounced () {
    var obj = this, args = arguments;
    function delayed () {
      func.apply(obj, args);
      timeout = null;
    }

    if (timeout) {
      clearTimeout(timeout);
    }

    timeout = setTimeout(delayed, 200);
  };
};

function resize() {
  var width = $('#lhs').width();
  var componentIds = ['kl','tl','tl1','tl2','minikl'];
  for(var i = 0; i < componentIds.length; i++) {
    var id = componentIds[i];
    var jq = $('#' + id);
    if(jq.length > 0) {
      if(id === 'minikl') {
        width = $('#rhs').width() - 40;
      }
      KeyLines.setSize(id, width, jq.height());
    }
  }
}

function fileSystemWarning () {
  var warning = $('<div>').addClass('alert').html('Warning! You are running this demo from the filesystem, this may cause some of the functionality to not work.' +
                                 ' It is recommended to serve this demo from a server. See <a href="README.txt">README.txt</a> for more details.');
  $('#rhscontent').prepend(warning);
}

$(window).resize(debounce(resize));

$(function () {
  if (location.protocol === 'file:') {
    fileSystemWarning();
  }
  resize();
});

var qLookup = {};

function reloadPage() {
  var params = [];
  for (var q in qLookup) {
    params.push(q + '=' + qLookup[q]);
  }
  var search = '?' + params.join('&');
  location.href = location.pathname + search;
}

function readQueryString(name) {
  qLookup = {};
  var search = window.location.search;
  if (search.length) {
    var queries = search.slice(1).split('&');
    for (var i = 0; i < queries.length; i++) {
      var split = queries[i].split('=');
      if (split.length) {
        qLookup[split[0]] = split.length === 2 ? split[1] : '';
      }
    }
  }
}

// on the server we use cookies to store the mode, but here we're using query string for simplicity
var readCookie = getParam;
function getParam(name) {
  return qLookup[name];
}

function setParam(name, value) {
  qLookup[name] = value;  
}

function setMode() {
  var mode = getParam('mode') || 'auto' ;

  if (KeyLines.webGLSupport() && $('#kl').length) {
    $('#webglButton').show();
    $('#webglButton').text(mode === 'webgl' ? 'WebGL On' : 'Try WebGL');
    $('#webglButton').toggleClass('active', mode === 'webgl');
  }
}

$(function() {
  if (location.protocol !== 'file:') {
    // WebGL doesn't run from the file system
    readQueryString();

    $('#webglButton').on('click', function(evt) {
      var mode = $(this).hasClass('active') ? 'auto' : 'webgl';
      setParam('mode', mode);
      reloadPage();
    });

    setMode();
  }
});
