/************************************/
/* Global variables and colours     */ 
/************************************/

// Chart variables
var chart;
var miniChart;

// Filter variables
var filterInProgress = false;
var refilter = false;
var maxdata = 1;
var maxdegree = 5;
var datarange = maxdata/2;
var degreerange = 1;

// Colour variables
var greyColour = 'rgb(145, 146, 149)',
    selColour = 'rgb(255, 0, 0)', //'rgb(0, 105, 214)'; // rgb(0, 174, 239)';
    hoverColour = 'rgb(255, 0, 0)', //'rgb(0, 105, 214)'; // 'rgb(0, 174, 239)';
    circleColour = 'rgb(253, 141, 60)',
    band1 = 'rgb(254, 217, 118)',
    band2 = 'rgb(253, 141, 60)',
    band3 = 'rgb(252, 78, 42)',
    band4 = 'rgb(177, 0, 38)';

//the colour map is used for the hover functionality to restore the original colour after the hover
var colourMap = {};

var zoomOptions = {animate:true, time: 350};

function doZoom(name) {
  chart.zoom(name, zoomOptions);
}

function doLayout(name, options, callback) {
  if (!options) {
    options = {
      ms:700,
      animate: true, 
      tidy: true, 
      fit: true
    };
  }
  if (!name) {
    name = 'standard'; 
  }
  chart.layout(name, options, callback);
}

/************************************/
/* Helper functions                 */ 
/************************************/

function contains(arr, obj){
  for( var i = 0; i < arr.length; i++ ){
    if(arr[i] === obj){
      return true;
    }
  }
}

function prepareQuery(template, name){
  // Use the new Cypher Endpoint for transactions (introduced with Neo4j 2.x)
  // Use the new Cypher Endpoint for transactions (introduced with Neo4j 2.x)
  return {
    statements: [{
      statement: template, 
      parameters: {name: name},
      resultDataContents: ['graph']
    }]
  };
}

function normalize(max, min, value){
  if (max === min) {
    return min;
  }
  else {
    return (value - min) / (max - min);
  }
}

function miniChartFilter(items, baseColour){
  var filtered = [],
      miniItem;
  for( var i = 0; i < items.length; i++){
    if(miniChart.getItem(items[i].id)){
      // It is a link
      if(items[i].w){
        miniItem = {id: items[i].id, w: items[i].w, c: baseColour ? circleColour : colourMap[items[i].id]};
      } else {
      // this will set some specific colours for miniChart
        miniItem = {id: items[i].id, e: items[i].e, c: baseColour ? circleColour : colourMap[items[i].id]};
      }
      filtered.push(miniItem);
    }
  }
  return filtered;
}

/************************************/
/* Load KeyLines and general setup  */
/************************************/

$(function () {
  KeyLines.paths({ assets: 'assets/',
    flash: { swf: 'swf/keylines.swf', swfObject: 'js/swfobject.js', expressInstall: 'swf/expressInstall.swf' }});
  
  var options = {
    logo: {u: ''}
  };
  KeyLines.create([
    // Big chart with options
    {id: 'kl', options: {handMode: true, hover: 5, navigation: {shown: false}, overview: {icon: false}, selectionColour: selColour}},
    // Small chart with options
    {id: 'minikl', options: {handMode: true, navigation: {shown: false}, overview: {icon: false}, selectionColour: selColour}}
  ], afterChartCreated);
  
});

function afterChartCreated(err, loadedCharts) {

  chart = loadedCharts[0];
  miniChart = loadedCharts[1];
  
  // Setup events
  setupEvents();
  
  // Chart bindings, includes hover and miniChart functionality
  chartBindings();
  
  // Initial query (get everything, lazy implementation)
  var query = prepareQuery('MATCH ()-[r:CONTACTS]-() RETURN r','');

  // Call the Cypher Endpoint and then load the data in KeyLines
  callCypher(query, function (json) {
    
    // Transform the items from the Neo4j format to the KeyLines one
    var items = makeKeyLinesItems(json);
    chart.load({type: 'LinkChart', items: items}, function() {
      chart.zoom('fit', {}, doLayout);
    });

  });

  chart.animateProperties(links, {}, function() {
      analysis(function(){
        miniChart.animateProperties(miniItems, {time: 500}); //fire and forget
      });
    });
  
}

function setupEvents() {
  // Custom overlay code:
  $('#home').click(function () {
    doZoom('fit');
  });
  $('#zoomIn').click(function () {
    doZoom('in');
  });
  $('#zoomOut').click(function () {
    doZoom('out');
  });
  $('#changeMode').click(function() {
    var hand = chart.options().handMode ? true : false; //be careful with undefined
    hand = !hand;
    chart.options({handMode: hand});
    $('#iconMode').toggleClass('fa-arrows fa-edit');
  });
  $('#layout').click(function (evt) {
    doLayout(evt.shiftKey ? 'structural': 'standard');
  });
  $('#fullScreenButton').click(function() {
    if(KeyLines.fullScreenCapable()) {
      KeyLines.toggleFullScreen($('.row')[0], function(isFullScreen) {
        if(isFullScreen) {
          KeyLines.setSize('kl', screen.width, screen.height);
          $('.span8').width(screen.width).height(screen.height);
          $('#rhs').hide();
        } else {
          $('.span8').width('').height('');
          $('#rhs').show();
          KeyLines.setSize('kl', $('#lhs').width(), 580);
        }
        // After resize, fit the chart to the window
        chart.zoom('fit');
      });
    }
  });
 
  // Scaling buttons
  $('.size').click(function() {
    $('.size').removeClass('active btn-kl');
    $(this).addClass('active btn-kl');
    
    analysis();
  });
  
  // Setup sliders
    $('#dataslider').slider({
    change: function(e, ui) {
      datarange = 1 - ui.value;
      $('#datalabel').text( Math.round(datarange*100) + '% by data');
      filterChart();
    },
    min: 0,
    value: maxdata/2,
    max: maxdata,
    step: 0.01
  });
  
  $('#degreeslider').slider({
    change: function(e, ui) {
      degreerange = ui.value;
      $('#degreelabel').text(' >= ' + degreerange);
      filterChart();
    },
    min: 1,
    value: 1,
    max: maxdegree,
    step: 1
  });
  
  // Only show the full screen icon if the browser is capable of it, and if 
  // we are using a canvas (not flash)
  $('#fullscreenlistitem').toggle(KeyLines.fullScreenCapable() && chart.canvas);
  
}

function chartBindings() {
  // This variable keeps hold of the id of the last hovered over thing
  var undoProperties;
  
  // Hover highlighting
  chart.bind('hover', function (id) {
    if (undoProperties) {
      chart.setProperties(undoProperties);
      undoProperties = null;
    }
    if (id) {
      var item = chart.getItem(id);
      var things;
      if (item.type === 'node') {
        things = chart.graph().neighbours(id);
        things.nodes.push(id); //add the item itself
      }
      else {
        things = {links:[id], nodes:[item.id1, item.id2]};
      }
      var props = doItems(things, true, id);
      chart.setProperties(props);
      undoProperties = doItems(things, false);
    }
  });
 
    function doItems(items, show, selectedId) {
    var nodes = doIdList(items.nodes, function (id) {
      return {
        id: id,
        fbc: show ? hoverColour : null,
        c: show ? ((id === selectedId) ? selColour: hoverColour) : colourMap[id]
      };
    });
    var links = doIdList(items.links, function (id) {
      return {
        id: id,
        fbc: show ? hoverColour : null,
        c: show ? ((id === selectedId) ? selColour: hoverColour) : null
      };
    });
    return nodes.concat(links);
  }
  
  function doIdList(ids, fn) {
    var propertyList = [];
    for (var i = 0; i < ids.length; i++) {
      propertyList.push(fn(ids[i]));
    }
    return propertyList;
  }
  
  var loadActive = false;
  var queuedUpItems;
  function loadMiniChart(items) {
    miniChart.load({
      type: 'LinkChart',
      items: items
    }, function () {
      miniChart.layout('standard', {}, function() {
        //do again if there are queued up items
        if (queuedUpItems) {
          var newVar = queuedUpItems;
          queuedUpItems = null;
          loadMiniChart(newVar);
        }
        else {
          loadActive = false;
        }
      });
    });
  }
  
  // Prevent link dragging
  chart.bind('dragstart', function (name) {
    //prevent curvy links being made
    if (name === 'offset') {
      return true;
    }
  });
  
  // Selection pushes items to miniChart
  chart.bind('selectionchange', function () {
    var ids = chart.selection();
    var items = [];
    function pushNode(node) {
      node.c = colourMap[node.id];
      node.fbc = undefined;
      node.x = 0;
      node.y = 0;
      items.push(node);
    }
    if (ids.length > 0) {
      
      //find nearest neighbours
      var neighbours = chart.graph().neighbours(ids);
      
      // Copy the items from the current chart over to the miniChart.
      // however, because the hover has changed the colours, we need to reset based on the non-hover colour
      chart.each({type:'node'}, function(node) {
        if (contains(ids, node.id)) {
          pushNode(node);
        }
        else {
          if (contains(neighbours.nodes, node.id)) {
            pushNode(node);
          }
        }
      });
      chart.each({type:'link'}, function(link) {
        if (contains(neighbours.links, link.id)) {
          link.c = colourMap[link.id];
          items.push(link);
        } else {
          if(contains(ids, link.id)){
            link.c = colourMap[link.id];
            items.push(link);
          }
        }
      });
    }
    //any selection events which arrive DURING the following calls should be buffered
    //this is to stop the 'bounding box' drag sending nodes across to the miniChart too fast!
    if (!loadActive) {
      loadActive = true;
      loadMiniChart(items);
    }
    else {
      queuedUpItems = items;  
      // note - this just overrides any previously queued items (deliberately)
    }
  });
  
}

/************************************/
/* Chart filtering                  */ 
/************************************/

function filterChart() {
  refilter = filterInProgress;
  if (refilter) {
    return;
  }
  filterInProgress = true;
  var checked = {};

  
  chart.filter(function(item) {
    var maxdata = $( "#dataslider" ).slider( "option", "max" );
    var maxdegree = $( "#degreeslider" ).slider( "option", "max" );
    if (item.d.type === 'destination') {
      return true;
    } else {
      return (item.d.properties['number'] <= datarange) && (item.d.properties['degree'] >= degreerange);
    }
  }, { type: 'node'}, function() {
    filterInProgress = false;
    if (refilter) {
      filterChart();
    }
    chart.layout();
  });
}

/*************************************/
/* Applying scaling and analysis     */
/*************************************/

function animateValues(values, baseColour){
  var max = _.max(_.values(values)),
      min = _.min(_.values(values)),
      items;
  
  var baseSizes = {};
  chart.each({type: 'node'}, function(node){
    var id = node.id,
        a = node.d.type === 'destination' ? 2.5 : 1;
    baseSizes[id] = a
  });
  
  items = _.map(values, function(value, key){
    // Normalise the value in the range 0 -> 1
    value = normalize(max, min, value);
    // enlarge nodes with higher values
    // sites have a larger base size
    var enlargement = (value * 4.5) + baseSizes[key];
    return {id: key, e: enlargement};
  });
  
  var miniItems = miniChartFilter(items, baseColour); 
  chart.animateProperties(items, {time: 500}, function(){
    doLayout('standard', {}, function(){
      miniChart.animateProperties(miniItems, {time: 500}, function(){
        miniChart.layout();
      });
    });
  });
  
  var links = linkAnimationSettings();
  
  var miniItems = miniChartFilter(links, false);
  chart.animateProperties(links, {}, function() {
      miniChart.animateProperties(miniItems, {time: 500}); //fire and forget
  });
}

function linkAnimationSettings() {
  var weight = weightName();
  var links = [];
  ({type:'link'}, function (link) {
    switch (weight) {
      case 'same':
        var enlargement = 10;
        break;
      case 'data':
//        var enlargement = 40*Math.log(link.d.properties[weight]+ 1);
        var enlargement = 10;
        break;
    }
    var colour = 'rgb(145, 146, 149)'; //midgrey
    links.push({id: link.id, w: enlargement});
  });
  return links;
}

function same(options, callback) {
  var sizes = {};
  chart.each({type: 'node'}, function(node){
    sizes[node.id] = 0;
  });
  callback(sizes);
}

function data(options, callback) {
  var sizes = {};
  chart.each({type: 'node'}, function(node){
    sizes[node.id] = node.d.type === 'destination' ? Math.log(node.d.properties['datareceived'] + 2) : node.d.properties['datasent']+1
  });
  callback(sizes);
}

function wrapCallback(fn) {
  return function(options, callback) {
    var result = fn(options);
    callback(result);
  };
}

function analysis(callback) {
  var options = {};
  
  var name = analysisName();
  var fn = analysisFunction(name);
  fn(options, function(values) {
    animateValues(values, name === 'datascaling');
    if (callback) {
      callback();
    }
  });
}

//returns the name of the type of analysis from the DOM
function analysisName() {
  if ($('#datascaling').hasClass('active')) {
    return 'datascaling';
  }
  return 'samescaling';
}

function weightName() {
  if ($('#datascaling').hasClass('active')) {
    return 'data';
  }
  if ($('#requestscaling').hasClass('active')) {
    return 'nrequests';
  }
  return 'same';
}

function analysisFunction(name) {
  switch (name) {
    case 'requestscaling':
      return requests;
    case 'closeness':
      return chart.graph().closeness;
    case 'betweenness':
      return chart.graph().betweenness;
    case 'pagescaling':
      return wrapCallback(chart.graph().pageRank);
    case 'eigenscaling':
      return wrapCallback(chart.graph().eigenCentrality);
    case 'datascaling':
      return data;
    case 'samescaling':
      return same;
  }
}

/*************************************/
/* Calling Neo4j to get data         */
/*************************************/

function makeKeyLinesItems(json){
  var items = [];

  $.each(json.results[0].data, function (i, entry){
    $.each(entry.graph.nodes, function (j, node){
      items.push(makeNode(node));
    });
    $.each(entry.graph.relationships, function (j, edge){
      items.push(makeLink(edge));
    });
  });

  return items;
}

function callCypher(query, callback) {
  $.ajax({
    type: 'POST',
    // Change this URL according to your Neo4j URL
    url:'http://localhost:7474/db/data/transaction/commit',
//    url:'http://ec2-52-17-39-61.eu-west-1.compute.amazonaws.com:7474/db/data/transaction/commit',
    data: JSON.stringify(query),

    // // If you're using Neo4j 2.2 or above uncomment the following 3 lines 
    // // and set your Database username and password:
     headers: {
       Authorization: 'Basic '+btoa('neo4j:wandera')
     },

    dataType: 'json',
    contentType: 'application/json'
  })
  .fail(reportError)
  .done(callback);

}

function reportError(jqXHR, _, errorString){
  // report the error code: very useful to debug some connection issues with Neo4J
  console.log('Error reported from Neo4J: '+ jqXHR.status + ' - ' + errorString );
}


/***********************************************************************/
/* Parsing Cypher response format to KeyLines chart format            */
/***********************************************************************/

function getType(labels){
  // it takes an Array of labels:
  // [Actor, Male]
  // pick the first one and return it downcase
  // "actor"
  return labels[0].toLowerCase() === 'destination' ? 'destination' : 'source';
}

function makeNode(item){
  var baseType = getType(item.labels);

  var id = item.id;

  var node = {
    id: id,
    type: 'node',
    t: item.properties.address.length >= 10 ? item.properties.address.substring(0,15) : item.properties.address,
    c: baseType === 'destination' ? '#5CCC65': '#0088CC',
    ci: true,
    e: 1,
    // save the neo4j item in case we need more info
    d: item
  };
  // add the baseType as well
  node.d.type = baseType;
  // add custom properties
  node.d.properties = item.properties
  
  // Save colour to colourMap
  colourMap[node.id] = node.c
  
  return node;
}

function makeLink(item) {
  var id1 = item.startNode,
      id2 = item.endNode;

  var labels = item.properties.roles;
  
  var link = {
  // Note: ids in Neo4j are unique within types: id collisions can happen between nodes and links!
  // Neo4j original id will be stored in the d property
  // In KeyLines we'll build a fake id based on the ends
    id: item.id + ':' + id1 + '-' + id2,
    type: 'link',
    id1: id1,
    id2: id2,
    t: labels ? labels.join(' ') : '',
    fc: 'rgba(52,52,52,0.9)',
    // draw an arrow pointing at the destination
    a2: true,
    c: greyColour,
    w: 2,
    // save the item for future use
    d: item
  };
  // add custom properties
  link.d.properties = item.properties
  
  return link;

}